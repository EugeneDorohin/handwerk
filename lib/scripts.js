const handwerk = {
    init: function () {
        $(document).on('mouseup', this.closeSelect.bind(this));
        $(document).on('click', '.drop_select .i', this.chooseSelect.bind(this));
    },
    goToBlock: function (block) {
        $('body, html').animate({ scrollTop: $('#' + block).offset().top - 100 }, 800);
        return;
    },
    showTeamPopup: function (id) {
        $('body').addClass('no_scroll');
        $('.popup_team .content').html( $('#' + id).html() );
        $('.popup_team').addClass('show');
    },
    closeTeamPopup: function () {
        $('.popup_team').removeClass('show');
        $('body').removeClass('no_scroll');
    },
    openSelect: function (e) {
        e.next('.drop_select').addClass('open');
    },
    closeSelect: function (e) {
        var container = $('.drop_select');
        if (container.has(e.target).length === 0) container.removeClass('open');
    },
    chooseSelect: function (e) {
        var target = $(e.currentTarget),
            value = target.text();
        $('.drop_select .i').removeClass('current');
        target.addClass('current');
        target.closest('.select').find('input').val(value);
        $('.drop_select').removeClass('open');
    }
};

$(document).ready(function () {
    handwerk.init();
});